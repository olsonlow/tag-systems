//
//  main.cpp
//  TagSystem2
//
//  Created by Lowell D. Olson on 8/24/14.
//  Copyright (c) 2014 Lowell D. Olson. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <cmath>
#include <iterator>

using namespace std;

bool findRepeats(vector<string>& strings);
string getBinary(unsigned integer);
string generateSeed(int seedLength);

int main(int argc, const char * argv[])
{
    vector<vector<string>>products;
    vector<long>positions;
    vector<long>maxLengths;
    vector<string>seeds;
    int seedLength;
    string ifZero = "00";
    string ifOne = "1101";
    string currentState = "";
    string biVal = "0";
    long lengthToKill = 3;
    long maxLength = 0;
    long currentLength = 0;
    
    cout << "Enter the string length of the seed ";
    cin >> seedLength;
    
    for(int p = 0; p < pow(2, seedLength) ; p++)
    {
        biVal = getBinary(p);
        currentState = generateSeed(seedLength);
        
        long lengthDiff = (currentState.length() - biVal.length());
        
        currentState = currentState.replace(lengthDiff, biVal.length(), biVal);
        seeds.push_back(currentState);
        products.push_back(vector<string>());
        bool stop = false;
        
        
        while(stop == false)
        {
            char firstValue = currentState.at(0);
            
            if(currentState.length() < lengthToKill)
            {
                products[p].push_back(currentState);
                if(currentLength > maxLength)
                    maxLengths.push_back(maxLength);
                break;
                
            }
            else if(firstValue == '1')
            {
                currentState.erase(0,3);
                currentState.append(ifOne);
                products[p].push_back(currentState);
                stop = findRepeats(products[p]);
                if(stop)
                    break;
            }
            else{
                currentState.erase(0,3);
                currentState.append(ifZero);
                products[p].push_back(currentState);
                stop = findRepeats(products[p]);
                if(stop)
                    break;
            }
            
            currentLength = currentState.length();
            if(currentLength > maxLength)
            {
                maxLength = currentLength;
            }
        }
        maxLengths.push_back(maxLength);
        maxLength = 0;
        currentLength = 0;
    }
    int counter = 1;
    string outputfile;
    ofstream output;
    cout << "Enter a filename i.e. 32bitseed.txt ";
    cin >> outputfile;
    output.open(outputfile);
    vector<vector<string> >::iterator it=products.begin(), end=products.end();
    while (it!=end)
    {
        //output << "Starting with seed of " << seeds[counter] << endl;
        //output << "The max stringlength for this set = " << maxLengths[counter] << endl;
        //output << "The number of strings in this set is = " << products[counter].size();
        //output << "\n";
        
        output << counter << ", " << seeds[counter] << ", " << maxLengths[counter] << ", " << products[counter].size() << endl;
        //vector<string>::iterator it1=it->begin(),end1=it->end();
        //copy(it1,end1,ostream_iterator<string>(output, " "));
        //output << endl << endl;
        ++it;
        ++counter;
    }
    
    return 0;
}

bool findRepeats(vector<string>& strings)
{
    long i = 0;
    long j = 1;
    
    for(; i < strings.size(); i++)
    {
        string currentString = strings[i];
        for(; j < strings.size() -1; j++)
        {
            string compareS =  strings[j];
            if(j == i)
                continue;
            if(currentString.compare(compareS) == 0)
                return true;
        }
        j=1;
    }
    return false;
}

string getBinary(unsigned integer)
{
    string s ="";
    do
    {
        s.push_back('0' + (integer & 1));
    } while (integer >>= 1);
    std::reverse(s.begin(), s.end());
    return s;
    
}
string generateSeed(int seedLength)
{
    string startingState = "";
    for(int i = 0; i < seedLength; i++){
        startingState.append("0");
    }
    return startingState;
}